<?php
$lang->service->common = 'Service';

$lang->service->index  = 'Accueil Service';
$lang->service->create = 'Ajout Service';
$lang->service->edit   = 'Editer Service';
$lang->service->view   = 'D�tails Service';
$lang->service->manage = 'G�rer Service';
$lang->service->delete = 'Suppr. Service';
$lang->service->list   = 'Service List';
$lang->service->browse = 'Browse Service';

$lang->service->name        = 'Service';
$lang->service->color       = 'Couleur';
$lang->service->version     = 'Version';
$lang->service->desc        = 'Description';
$lang->service->dept        = 'Compartiment';
$lang->service->devel       = 'D�veloppeur';
$lang->service->qa          = 'QA';
$lang->service->ops         = 'OPS';
$lang->service->host        = 'Host';
$lang->service->type        = 'Type';
$lang->service->softName    = 'Software';
$lang->service->softVersion = 'Version';
$lang->service->createdBy   = 'Cr�� par';
$lang->service->createdDate = 'Cr�� le';
$lang->service->editedBy    = 'Edit� par';
$lang->service->editedDate  = 'Edit� le';
$lang->service->parent      = 'ID Parent';
$lang->service->grade       = 'Grade';
$lang->service->order       = 'Ordre';
$lang->service->deleted     = 'Supprim';
$lang->service->colorTag    = 'Couleur';
$lang->service->external    = 'Service Type';
$lang->service->port        = 'Port';
$lang->service->entry       = 'Entry';
$lang->service->deploy      = 'Deploy Path';
$lang->service->isTrue      = 'True';
$lang->service->isFalse     = 'False';

$lang->service->empty = 'No Service';

$lang->service->externalList['0']   = 'Internal Service';
$lang->service->externalList['1']   = 'External Service';
$lang->service->typeList['service']   = 'Service';
$lang->service->typeList['component'] = 'Composant';

$lang->service->all = 'Tous Services';
$lang->service->openedbyme = 'Ouverts par moi';
$lang->service->ownerbyme  = 'Mes services';

$lang->service->basicInfo    = 'Information de Base';
$lang->service->detail       = 'D�tail';
$lang->service->createTop    = 'Ajout Top Service';
$lang->service->createDepend = 'Ajout Service D�pendant';
$lang->service->createPeer   = 'Ajout Service Associ';
$lang->service->listView     = 'List';
$lang->service->TreeView     = 'Tree';

$lang->service->confirmDelete = 'Voulez-vous supprimer ce service ?';
$lang->service->errorParent   = "%s ne peut pas �tre ajout� sous un composant. Le composant est la plus petite unit� !";
$lang->service->errorType     = "Ce service n'est pas au niveau le plus bas et ne peut pas �tre modifi� en tant que type de composant.";
